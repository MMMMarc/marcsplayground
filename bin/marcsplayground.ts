#!/usr/bin/env node
import * as cdk from '@aws-cdk/core';
import { MarcsplaygroundStack } from '../lib/marcsplayground-stack';

const app = new cdk.App();
new MarcsplaygroundStack(app, 'MarcsplaygroundStack');
